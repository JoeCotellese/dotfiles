# Dot Files

My dotfiles. There isn't anything special here. The only thing to note is that I'm following the process described in this post from [Atlassian](https://www.atlassian.com/git/tutorials/dotfiles)
